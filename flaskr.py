#coding: utf-8
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, request, session,  redirect, url_for, abort, render_template, flash, _app_ctx_stack

app = Flask(__name__)
app.config.from_object('config')

def init_db():
    '''Create database tables.'''
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode = "r") as f:
            db.cursor().executescript(f.read())
        db.commit()

def get_db():
    '''Opens a database connection when there is none yet for the 
    current application context.
    '''
    top = _app_ctx_stack.top
    if not hasattr(top, 'sqlite_db'):
        sqlite_db = sqlite3.connect(app.config['DATABASE'])
        sqlite_db.row_factory = sqlite3.Row
        top.sqlite_db = sqlite_db

    return top.sqlite_db

@app.teardown_appcontext
def colse_db_connection(exception):
    '''Close the database again at the end of the request.'''
    top = _app_ctx_stack.top
    if hasattr(top, 'sqlite_db'):
        top.sqlite_db.close()

@app.route('/')
def show_entries():
    db=get_db()
    cur = db.execute('SELECT title, text FROM entries ORDER BY id DESC')
    entries = cur.fetchall()
    return render_template('show_entries.html', entries = entries)

@app.route('/add', methods = ['POST'])
def add_entry():
    if not session.get('logged_in'):
        abort(401)
    db = get_db()
    db.execute('INSERT INTO entries (title, text) VALUES (?, ?)',
            [request.form['title'], request.form['text']])
    db.commit()
    flash('New entry has succesfully posted!')
    return redirect(url_for('show_entries'))

@app.route('/delete', methods = ['GET','POST'])
def delete_entries():
    if not session.get('logged_in'):
        flash('Please log in first!')
        return redirect(url_for('show_entries'))
    db = get_db()
    if request.method == 'POST':
        form = request.form
        for entry in form:
            # print entry
            try:
                order = 'DELETE FROM entries WHERE title=%s'% str(entry)
                db.execute(order)
            except:
                print entry + 'Delete fail!'
        flash('Articles deleted!')
        return redirect(url_for('show_entries'))
    cur = db.execute('SELECT title, text FROM entries ORDER BY id DESC')
    entries = cur.fetchall()
    return render_template('delete_entries.html', entries = entries)

@app.route('/login', methods = ['POST', 'GET'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username!'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password!'
        else:
            session['logged_in'] = True
            flash('You have logged in!')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You have logged out!')
    return redirect(url_for('show_entries'))

if __name__ == '__main__':
    init_db()
    app.run()
